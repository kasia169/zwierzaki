package pl.bartek.dec;

public class KindOffFeed implements Comparable<KindOffFeed>{
    private Feed food;
    int quantity;

    public KindOffFeed(int quantity) {
        this.quantity = quantity;
    }


    public KindOffFeed(Feed food) {
        this.food = food;
    }

    public Feed getFood() {
        return food;
    }

    public void setFood(Feed food) {
        this.food = food;
    }

    @Override
    public int compareTo(KindOffFeed o) {
        return this.getFood().compareTo(o.getFood());
    }
}
