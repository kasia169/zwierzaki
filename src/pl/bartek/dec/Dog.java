package pl.bartek.dec;

import java.util.List;

public class Dog extends Animal {
    public Dog(int age, String name, TypeOfAnimal type, int numberOfLimb, List<Integer> allergies, List<KindOffFeed> kindOffFeeds) {
        super(age, name, type, numberOfLimb, allergies, kindOffFeeds);
    }
}
