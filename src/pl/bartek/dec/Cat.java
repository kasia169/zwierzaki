package pl.bartek.dec;

import java.util.List;

public class Cat extends Animal{
    public Cat(int age, String name, TypeOfAnimal type, int numberOfLimb, List<Integer> allergies, List<KindOffFeed> kindOffFeeds) {
        super(age, name, type, numberOfLimb, allergies, kindOffFeeds);
    }
}
