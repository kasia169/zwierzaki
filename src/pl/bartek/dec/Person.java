package pl.bartek.dec;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private String surname;
    private int age;
    private Sex sex;
    private Adress adres;
    private List<Animal> myAnimals;
    private List<Integer> allergies;
    private List<KindOffFeed> availableFood;

    public Person() {
        this.myAnimals = new ArrayList<Animal>();
        this.allergies = new ArrayList<Integer>();
        this.availableFood = new ArrayList<KindOffFeed>();
    }

    public List<KindOffFeed> getAvailableFood() {
        return availableFood;
    }

    public void setAvailableFood(List<KindOffFeed> availableFood) {
        this.availableFood = availableFood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Adress getAdres() {
        return adres;
    }

    public void setAdres(Adress adres) {
        this.adres = adres;
    }

    public List<Animal> getMyAnimals() {
        return myAnimals;
    }

    public void setMyAnimals(List<Animal> myAnimals) {
        this.myAnimals = myAnimals;
    }

    public List<Integer> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<Integer> allergies) {
        this.allergies = allergies;
    }

    public void feedAnimals() {
        for (Animal animal : myAnimals) {
            boolean flag = true;

            for (int i = 0; i < animal.getKindOffFeeds().size(); i++) {
                if (doIHaveSuchKindOfFood(animal.getKindOffFeeds().get(i))) {
                    animal.feedMe(this.availableFood.get(this.availableFood.indexOf(animal.getKindOffFeeds().get(i))));
                    System.out.println("Nakarmiłem zwierze " + animal.getName());
                    flag = false;
                    break;
                }
            }
            if (flag) {
                System.out.println("Nie mam jedzenia, które lubisz");
            }
        }
    }
    private boolean doIHaveSuchKindOfFood(KindOffFeed food) {
        for (int i = 0; i < this.getAvailableFood().size(); i++) {
            if (food.getFood().compareTo(this.getAvailableFood().get(i).getFood()) == 0) {
                return true;
            }
        }
        return false;
    }

    public boolean canHaveAnimal(Animal animal) {
        for (int i = 0; i < animal.getAllergies().size(); i++) {
            if (animal.getAllergies().get(i) == this.allergies.get(i)) {
                return false;
            }
        }
        return true;
    }
}
