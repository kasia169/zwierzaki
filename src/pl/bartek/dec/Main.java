package pl.bartek.dec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        List<KindOffFeed> jedzeniePsa = new ArrayList<KindOffFeed>();
//        jedzeniePsa.add(new KindOffFeed("kość"));
//        jedzeniePsa.add(new KindOffFeed("mieso"));
//        jedzeniePsa.add(new KindOffFeed("karma w puszcze"));
//        jedzeniePsa.add(new KindOffFeed("karma sucha"));
//
//        List<Integer> alergiePsa = new ArrayList<Integer>();
//        alergiePsa.add(1);
//
//        Animal pies = new Dog(3, "burek", TypeOfAnimal.MAMMAL, 4, alergiePsa, jedzeniePsa);
//
//        KindOffFeed obiad = new KindOffFeed("kość");
//
//        System.out.println(jedzeniePsa.get(2).getFood());
//        System.out.println(pies.feedMe(obiad));

        Person person = new Person();
        person.setName("Maciek");
        person.setSurname("Kowalski");
        person.setAge(28);
        person.setSex(Sex.MALE);



        List <Integer> allergies = new ArrayList<>();
        allergies.add(1);
        allergies.add(2);
        allergies.add(3);

        List <KindOffFeed> availableFood = new ArrayList<>();
        availableFood.add(new KindOffFeed(Feed.bread));
        availableFood.add(new KindOffFeed(Feed.meat));
        availableFood.add(new KindOffFeed(Feed.water));

        List <KindOffFeed> dogFeed = new ArrayList<>();
        dogFeed.add(new KindOffFeed(Feed.bread));
        dogFeed.add(new KindOffFeed(Feed.meat));
        dogFeed.add(new KindOffFeed(Feed.water));

        List <KindOffFeed> catFeed = new ArrayList<>();
        catFeed.add(new KindOffFeed(Feed.fish));
        catFeed.add(new KindOffFeed(Feed.milk));
        catFeed.add(new KindOffFeed(Feed.water));

        List <KindOffFeed> birdFeed = new ArrayList<>();
        birdFeed.add(new KindOffFeed(Feed.worm));
        birdFeed.add(new KindOffFeed(Feed.water));

        List<Animal> zwierzeta = new ArrayList<Animal>();

        Animal cat = new Cat(3, "Filemon", TypeOfAnimal.MAMMAL, 5, allergies, catFeed);
        Animal dog = new Dog(10, "Luna", TypeOfAnimal.MAMMAL, 10, allergies, dogFeed );
        Animal bird = new Bird(1, "Leon", TypeOfAnimal.BIRD, 7, allergies, birdFeed );

        zwierzeta.add(dog);
        zwierzeta.add(cat);
        zwierzeta.add(bird);
        person.setMyAnimals(zwierzeta);

        System.out.println(dog.printFood());
             System.out.println(dogFeed.get(2).getFood());



    }
}
