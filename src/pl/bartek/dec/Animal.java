package pl.bartek.dec;

import java.util.List;

public abstract class Animal {
    private int age;
    private String name;
    private TypeOfAnimal type;
    private int numberOfLimb;
    private List<Integer> allergies;
    private List<KindOffFeed> kindOffFeeds;


    public Animal(int age, String name, TypeOfAnimal type, int numberOfLimb, List<Integer> allergies, List<KindOffFeed> kindOffFeeds) {
        this.age = age;
        this.name = name;
        this.type = type;
        this.numberOfLimb = numberOfLimb;
        this.allergies = allergies;
        this.kindOffFeeds = kindOffFeeds;
    }

    public List<KindOffFeed> getKindOffFeeds() {
        return kindOffFeeds;
    }

    public void setKindOffFeeds(List<KindOffFeed> kindOffFeeds) {
        this.kindOffFeeds = kindOffFeeds;
    }

    public List<Integer> getAllergies() {
        return allergies;
    }

    public void setAllergies(List<Integer> allergies) {
        this.allergies = allergies;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Enum<TypeOfAnimal> getType() {
        return type;
    }

    public void setType(TypeOfAnimal type) {
        this.type = type;
    }

    public int getNumberOfLimb() {
        return numberOfLimb;
    }

    public void setNumberOfLimb(int numberOfLimb) {
        this.numberOfLimb = numberOfLimb;
    }

    public boolean feedMe(KindOffFeed kindOffFeed) {
        for (int i = 0; i < this.kindOffFeeds.size(); i++) {
            if (kindOffFeed.getFood() == (this.kindOffFeeds.get(i).getFood())) {

                return true;
            }
        }
        return false;
    }

        public String printFood(){
        String string = "";
           for (KindOffFeed feed : kindOffFeeds) {
               string += feed.getFood().toString() + " ";
           }
           return string;

    }
}
