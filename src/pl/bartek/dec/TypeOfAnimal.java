package pl.bartek.dec;

public enum TypeOfAnimal {
    BIRD, MAMMAL, FISH;
}
